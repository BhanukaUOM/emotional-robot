Emotional Robot


This robot is capable of determining emotions
and displaying a relevant emoticons on an LED
matrix. Emotions will be extracted from a voice
stream using voice to text conversion process.

Main devices and modules: Raspberry PI, LED  matrix, Shift registers, Mic, USB Sound Card
